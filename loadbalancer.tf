#Creating public IP for LoadBalancer
resource "azurerm_public_ip" "load_ip" {
  name                = "load-ip"
  resource_group_name = local.resource_group
  location            = local.location
  allocation_method   = "Static"
  zones = ["1","2"]
  sku = "Standard"
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}
#Creating LoadBalancer
resource "azurerm_lb" "app_balancer" {
  name                = "app-balancer"
  location            = local.location
  resource_group_name = local.resource_group
  frontend_ip_configuration {
    name                 = "frontend-ip"
    public_ip_address_id = azurerm_public_ip.load_ip.id
  }
  sku = "Standard"
  depends_on = [
    azurerm_public_ip.load_ip
  ]
}
#Creating backend pool for LoadBalancer
resource "azurerm_lb_backend_address_pool" "pool1" {
  loadbalancer_id = azurerm_lb.app_balancer.id
  name            = "pool1"
  depends_on = [
    azurerm_lb.app_balancer
  ]
}
#Attach backend pool to VMs
resource "azurerm_lb_backend_address_pool_address" "appvm1_adress" {
  name                    = "appvm1"
  backend_address_pool_id = azurerm_lb_backend_address_pool.pool1.id
  virtual_network_id      = azurerm_virtual_network.app_network.id
  ip_address              = azurerm_network_interface.app_interface_1.private_ip_address
  depends_on = [
    azurerm_lb_backend_address_pool.pool1
  ]
}

resource "azurerm_lb_backend_address_pool_address" "appvm2_adress" {
  name                    = "appvm2"
  backend_address_pool_id = azurerm_lb_backend_address_pool.pool1.id
  virtual_network_id      = azurerm_virtual_network.app_network.id
  ip_address              = azurerm_network_interface.app_interface_2.private_ip_address
  depends_on = [
    azurerm_lb_backend_address_pool.pool1
  ]
}
#Creating Health Probe
resource "azurerm_lb_probe" "probe1" {
  loadbalancer_id = azurerm_lb.app_balancer.id
  name            = "probe1"
  port            = 80
  depends_on = [
    azurerm_lb.app_balancer
  ]
}
#Creating rile for LoadBalancer
resource "azurerm_lb_rule" "rule1" {
  loadbalancer_id                = azurerm_lb.app_balancer.id
  name                           = "rule1"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "frontend-ip"
  backend_address_pool_ids = [ azurerm_lb_backend_address_pool.pool1.id ]
  probe_id = azurerm_lb_probe.probe1.id
  depends_on = [
    azurerm_lb.app_balancer,
    azurerm_lb_probe.probe1
  ]
}