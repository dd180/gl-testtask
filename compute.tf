#Creating VMs
resource "azurerm_windows_virtual_machine" "app_vm1" {
  name                = "appvm1"
  resource_group_name = local.resource_group
  location            = local.location
  size                = "Standard_B2s"
  admin_username      = "demousr"
  admin_password      = azurerm_key_vault_secret.vmpassword.value
  zone = 1
  network_interface_ids = [
    azurerm_network_interface.app_interface_1.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
  depends_on = [
    azurerm_network_interface.app_interface_1,
    azurerm_key_vault_secret.vmpassword
  ]
}

resource "azurerm_windows_virtual_machine" "app_vm2" {
  name                = "appvm2"
  resource_group_name = local.resource_group
  location            = local.location
  size                = "Standard_B2s"
  admin_username      = "demousr"
  admin_password      = azurerm_key_vault_secret.vmpassword.value
  zone = 2
  network_interface_ids = [
    azurerm_network_interface.app_interface_2.id,
  ]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
  depends_on = [
    azurerm_network_interface.app_interface_2,
    azurerm_key_vault_secret.vmpassword
  ]
}