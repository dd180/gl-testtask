Set-Item WSMan:\localhost\Client\TrustedHosts –Value "<IP>"
Enter-PSSession –ComputerName "<IP>" -Credential Get-Credential

Import-Module servermanager
Add-WindowsFeature Web-Scripting-Tools
Install-WindowsFeature -name Web-Server -IncludeManagementTools
Install-WindowsFeature Web-Asp-Net45
Install-WindowsFeature NET-Framework-Features
Import-Module "WebAdministration"

Get-Website | Remove-Website
Get-IISAppPool | Remove-WebAppPool
Get-ChildItem C:\inetpub\wwwroot\ | ForEach  { $_.Delete()}

New-Item C:\inetpub\wwwroot\TestTask -type Directory
Set-Content C:\inetpub\wwwroot\TestTask\index.htm "GlobalLogic Test Task"
New-Item IIS:\AppPools\TestTaskAppPool
New-Item IIS:\Sites\TestTask -physicalPath C:\inetpub\wwwroot\TestTask -bindings @{protocol="http";bindingInformation=":8080:"}
Set-ItemProperty IIS:\Sites\TestTask -name applicationPool -value TestTaskAppPool
exit

Set-Item WSMan:\localhost\Client\TrustedHosts –Value "<IP>"
Enter-PSSession –ComputerName "<IP>" -Credential Get-Credential

Import-Module servermanager
Add-WindowsFeature Web-Scripting-Tools
Install-WindowsFeature -name Web-Server -IncludeManagementTools
Install-WindowsFeature Web-Asp-Net45
Install-WindowsFeature NET-Framework-Features
Import-Module "WebAdministration"

Get-Website | Remove-Website
Get-IISAppPool | Remove-WebAppPool
Get-ChildItem C:\inetpub\wwwroot\ | ForEach  { $_.Delete()}

New-Item C:\inetpub\wwwroot\TestTask -type Directory
Set-Content C:\inetpub\wwwroot\TestTask\index.htm "GlobalLogic Test Task"
New-Item IIS:\AppPools\TestTaskAppPool
New-Item IIS:\Sites\TestTask -physicalPath C:\inetpub\wwwroot\TestTask -bindings @{protocol="http";bindingInformation=":8080:"}
Set-ItemProperty IIS:\Sites\TestTask -name applicationPool -value TestTaskAppPool
exit