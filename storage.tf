#Creating Storage
resource "azurerm_storage_account" "appstore" {
  name                     = "appstore13213213"
  resource_group_name      = local.resource_group
  location                 = local.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}

resource "azurerm_storage_container" "data" {
  name                  = "data"
  storage_account_name  = azurerm_storage_account.appstore.name
  container_access_type = "blob"
  depends_on = [
    azurerm_storage_account.appstore
  ]
}

resource "azurerm_storage_blob" "WinRM" {
  name                   = "WinRM.ps1"
  storage_account_name   = azurerm_storage_account.appstore.name
  storage_container_name = azurerm_storage_container.data.name
  type                   = "Block"
  source                 = "WinRM.ps1"
  depends_on = [
    azurerm_storage_container.data
  ]
}
#Creating and attaching data disks for VMs
resource "azurerm_managed_disk" "data_disk_1" {
  name                 = "data-disk_1"
  location             = local.location
  resource_group_name  = local.resource_group
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 4
  zone = 1
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}

resource "azurerm_virtual_machine_data_disk_attachment" "disk_attach_1" {
  managed_disk_id    = azurerm_managed_disk.data_disk_1.id
  virtual_machine_id = azurerm_windows_virtual_machine.app_vm1.id
  lun                = "0"
  caching            = "ReadWrite"
  depends_on = [
    azurerm_windows_virtual_machine.app_vm1,
    azurerm_managed_disk.data_disk_1
  ]
}

resource "azurerm_managed_disk" "data_disk_2" {
  name                 = "data-disk_2"
  location             = local.location
  resource_group_name  = local.resource_group
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 4
  zone = 2
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}

resource "azurerm_virtual_machine_data_disk_attachment" "disk_attach_2" {
  managed_disk_id    = azurerm_managed_disk.data_disk_2.id
  virtual_machine_id = azurerm_windows_virtual_machine.app_vm2.id
  lun                = "0"
  caching            = "ReadWrite"
  depends_on = [
    azurerm_windows_virtual_machine.app_vm2,
    azurerm_managed_disk.data_disk_2
  ]
}