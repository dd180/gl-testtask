#Creating Resource Group
resource "azurerm_resource_group" "app_grp" {
  name=local.resource_group
  location = local.location
}
#Creating VNet
resource "azurerm_virtual_network" "app_network" {
  name                = "app-network"
  location            = local.location
  resource_group_name = azurerm_resource_group.app_grp.name
  address_space       = ["10.0.0.0/16"]
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}
#Creating Subnet
resource "azurerm_subnet" "subnet1" {
  name                 = "subnet1"
  resource_group_name  = local.resource_group
  virtual_network_name = azurerm_virtual_network.app_network.name
  address_prefixes     = ["10.0.1.0/24"]
  depends_on = [
    azurerm_virtual_network.app_network
  ]
}
#Creating public IP form VMs to connect via WinRM, there was an attempt to connect through a private ip through the bastion
resource "azurerm_public_ip" "vm1ip" {
  name                = "vm1-ip"
  resource_group_name = local.resource_group
  location            = local.location
  allocation_method   = "Static"
  zones = ["1"]
  sku = "Standard"
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}

resource "azurerm_public_ip" "vm2ip" {
  name                = "vm2-ip"
  resource_group_name = local.resource_group
  location            = local.location
  allocation_method   = "Static"
  zones = ["2"]
  sku = "Standard"
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}
#Creating Network Interfaces for VMs
resource "azurerm_network_interface" "app_interface_1" {
  name                = "app-interface-1"
  location            = local.location
  resource_group_name = local.resource_group
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.vm1ip.id
  }
  depends_on = [
    azurerm_subnet.subnet1
  ]
}

resource "azurerm_network_interface" "app_interface_2" {
  name                = "app-interface-2"
  location            = local.location
  resource_group_name = local.resource_group
  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id = azurerm_public_ip.vm2ip.id
  }
  depends_on = [
    azurerm_subnet.subnet1
  ]
}