#Creating extension for running WinRM while creating VMs
resource "azurerm_virtual_machine_extension" "vm1_extension" {
  name                 = "appvm-extension-1"
  virtual_machine_id   = azurerm_windows_virtual_machine.app_vm1.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"
  depends_on = [
    azurerm_storage_blob.WinRM,
    azurerm_windows_virtual_machine.app_vm1,
    azurerm_virtual_machine_data_disk_attachment.disk_attach_1
  ]
  settings = <<SETTINGS
    {
        "fileUris": ["https://appstore13213213.blob.core.windows.net/data/WinRM.ps1"],
        "commandToExecute": "powershell -ExecutionPolicy Unrestricted -File WinRM.ps1"
    }
SETTINGS

}

resource "azurerm_virtual_machine_extension" "vm2_extension" {
  name                 = "appvm-extension-2"
  virtual_machine_id   = azurerm_windows_virtual_machine.app_vm2.id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.9"
  depends_on = [
    azurerm_storage_blob.WinRM,
    azurerm_windows_virtual_machine.app_vm2,
    azurerm_virtual_machine_data_disk_attachment.disk_attach_2
  ]
  settings = <<SETTINGS
    {
        "fileUris": ["https://appstore13213213.blob.core.windows.net/data/WinRM.ps1"],
        "commandToExecute": "powershell -ExecutionPolicy Unrestricted -File WinRM.ps1"
    }
SETTINGS

}