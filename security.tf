#Data for vault
data "azurerm_client_config" "current" {
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}
#Creating Network Security Group
resource "azurerm_network_security_group" "app_nsg" {
  name                = "app-nsg"
  location            = local.location
  resource_group_name = local.resource_group
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}
#Creating Network Security Group rules
#Listening port 80
resource "azurerm_network_security_rule" "Allow_HTTP" {
  name                        = "Allow_HTTP"
  priority                    = 200
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = local.resource_group
  network_security_group_name = azurerm_network_security_group.app_nsg.name
  depends_on = [
    azurerm_network_security_group.app_nsg
  ]
}

#Allow WinRM connection via HTTP
resource "azurerm_network_security_rule" "WinRM_HTTP" {
  name                        = "WinRM_HTTP"
  priority                    = 102
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "5985"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = local.resource_group
  network_security_group_name = azurerm_network_security_group.app_nsg.name
  depends_on = [
    azurerm_network_security_group.app_nsg
  ]
}
#Allow WinRM connection via HTTPS
resource "azurerm_network_security_rule" "WinRM_HTTPS" {
  name                        = "WinRM_HTTPS"
  priority                    = 103
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "5986"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = local.resource_group
  network_security_group_name = azurerm_network_security_group.app_nsg.name
  depends_on = [
    azurerm_network_security_group.app_nsg
  ]
}
#Allow RDP connection
resource "azurerm_network_security_rule" "RDP" {
  name                        = "RDP"
  priority                    = 210
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "3389"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = local.resource_group
  network_security_group_name = azurerm_network_security_group.app_nsg.name
  depends_on = [
    azurerm_network_security_group.app_nsg
  ]
}
#Attach NSG to Subnet
resource "azurerm_subnet_network_security_group_association" "nsg_association" {
  subnet_id                 = azurerm_subnet.subnet1.id
  network_security_group_id = azurerm_network_security_group.app_nsg.id
  depends_on = [
    azurerm_network_security_group.app_nsg
  ]
}
#Cearting keyvalut
resource "azurerm_key_vault" "app_vault" {
  name                        = "appvault484531"
  location                    = local.location
  resource_group_name         = local.resource_group
  enabled_for_disk_encryption = true
  tenant_id                   = data.azurerm_client_config.current.tenant_id
  soft_delete_retention_days  = 7
  purge_protection_enabled    = false

  sku_name = "standard"

  access_policy {
    tenant_id = data.azurerm_client_config.current.tenant_id
    object_id = data.azurerm_client_config.current.object_id

    key_permissions = [
      "Backup",
      "Create",
      "Decrypt",
      "Delete",
      "Encrypt",
      "Get",
      "Import",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Sign",
      "UnwrapKey",
      "Update",
      "Verify",
      "WrapKey",
    ]

    secret_permissions = [
      "Backup",
      "Delete",
      "Get",
      "List",
      "Purge",
      "Recover",
      "Restore",
      "Set",
    ]

    storage_permissions = [
      "Get",
    ]
  }
  depends_on = [
    azurerm_resource_group.app_grp
  ]
}
#Cearting sectet VMs password
resource "azurerm_key_vault_secret" "vmpassword" {
  name         = "vmpassword"
  value        = "Qwerty12345678"
  key_vault_id = azurerm_key_vault.app_vault.id
  depends_on = [
    azurerm_key_vault.app_vault
  ]
}